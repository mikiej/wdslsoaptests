﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoapWSDL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapWSDL
{
    public class SampleService : ISampleService
    {
        public string Test(string s)
        {
            Console.WriteLine("Test Method Executed!");
            return s;
        }

        /*
         * <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <Test xmlns="http://tempuri.org/">
                  <s>Hellow world</s>
                </Test>
              </soap:Body>
            </soap:Envelope>
        */

        /*pass in xml
         * <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <Test xmlns="http://tempuri.org/">
                  <s><![CDATA[<test>hello</test>]]></s>
                </Test>
              </soap:Body>
            </soap:Envelope>
        */


        public MyCustomModel TestJson(string s)
        {

            var temp = JsonConvert.DeserializeObject<MyCustomModel>(s);

            return temp;
        }


        /*
         * <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <TestJson xmlns="http://tempuri.org/">
                  <s>{"Id":1,"Name":"Dave","Email":"Dave@gmail.com"}</s>
                </TestJson>
              </soap:Body>
            </soap:Envelope>
        */


        public void XmlMethod(XElement xml)
        {
            Console.WriteLine(xml.ToString());
        }
        public MyCustomModel TestCustomModel(MyCustomModel customModel)
        {
            return customModel;
        }

        public MyCustomModel GetNewCustomModel()
        {
            return new MyCustomModel()
            {
                Id = 1,
                Name = "john",
                Email = "john@gmail.com"
            };
        }
        /*
         * <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <GetNewCustomModel xmlns="http://tempuri.org/">
                </GetNewCustomModel>
              </soap:Body>
            </soap:Envelope>         
         * */

    }
}
